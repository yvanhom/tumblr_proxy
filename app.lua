package.path = package.path .. ";./libs/?.lua"

local lapis = require("lapis")
local date = require("date")
local app = lapis.Application()

app:enable("etlua")
app.layout = require "views.layout"

app.cookie_attributes = function(self)
  local expires = date(true):adddays(30):fmt("${http}")
  return "Expires=" .. expires .. "; Path=/; HttpOnly"
end

app:get("index", "/(page:pid)", function(self)
  return require("controls.index")(self)
end)

app:get("oauth_index", "/oauth", function(self)
  return { render = "oauth" }
end)

app:post("oauth_index", "/oauth", function(self)
  return require("controls.oauth_set")(self)
end)

app:get("oauth_start", "/oauth/start", function(self)
  return require("controls.oauth_start")(self)
end)

app:get("oauth_confirm", "/oauth/confirm", function(self)
  return require("controls.oauth_confirm")(self)
end)

app:get("oauth_getcache", "/oauth/getcache", function(self)
  return require("controls.oauth_getcache")(self)
end)

app:get("logout", "/logout", function(self)
  return require("controls.logout")(self)
end)

app:get("proxy", "/proxy/:url", function(self)
  return require("controls.proxy")(self)
end)

app:post("search", "/search", function(self)
  return require("controls.search")(self)
end)

app:get("tag", "/tag/:tag(/b:ts)", function(self)
  return require("controls.tag")(self)
end)

app:get("blog", "/blog/:name(/page:pid)", function(self)
  return require("controls.blog")(self)
end)

app:post("like", "/post/like/:post", function(self)
  return require("controls.like")(self, true)
end)

app:post("unlike", "/post/unlike/:post", function(self)
  return require("controls.like")(self, true)
end)

app:post("follow", "/blog/:blog/follow", function(self)
  return require("controls.follow")(self, true)
end)

app:post("unfollow", "/blog/:blog/unfollow", function(self)
  return require("controls.follow")(self, false)
end)
return app
