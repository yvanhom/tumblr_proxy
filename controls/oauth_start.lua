local http = require "resty.http"
local mu = require "myutils"

local function oauth_start(req)
  local oa = mu.getOAuth()
  local httpc = http.new()
  local status, err = oa:updateRequestToken(httpc)
  if status then
    req.session.token = oa.oauth_token
    req.session.token_secret = oa.oauth_token_secret
    return { redirect_to = oa:getAuthorizeURL() }
  else
    return mu.returnError(req, "Can't update request token: " .. err)
  end
end

return oauth_start