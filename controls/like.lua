local json = require "cjson"
local basexx = require "basexx"
local tumblr = require "tumblr"
local http = require "resty.http"
local mu = require "myutils"

local function like(req, islike)
  local oa = mu.getOAuth(req)
  
  local likekey = json.decode(basexx.from_url64(req.params.post))
  if not likekey[1] or not likekey[2] then
    return { json = { status = "FAIL", errmsg = "Unknown likekey", data = likekey } }
  end
  
  local args = { id = likekey[1], reblog_key = likekey[2] }
  
  local res, err;
  if islike then
    res, err = tumblr.postLike(http.new(), oa, args)
  else
    res, err = tumblr.postUnlike(http.new(), oa, args)
  end
  if not res then
    return { json = { status = "FAIL", errmsg = err } }
  end
  
  return { json = { status = "SUCCESS" } }
end

return like