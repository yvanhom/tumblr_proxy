local mycache = require "mycache"

local function oauth_set(req)
  req.session.token = req.params.intoken
  req.session.token_secret = req.params.insecret
  mycache.setTokenSecret(oa.oauth_token, oa.oauth_token_secret)
  
  return { redirect_to = req:url_for("index") }
end

return oauth_set