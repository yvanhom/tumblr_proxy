local http = require "resty.http"
local mu = require "myutils"
local mycache = require "mycache"

local function oauth_confirm(req)
  local oa = mu.getOAuth(req)
  if not oa.oauth_token or not oa.oauth_token_secret then
    return mu.returnError(req, "TOKEN or TOKEN SECRET is empty!")
  end
  
  local token = req.params.oauth_token
  local verifier = req.params.oauth_verifier
  if token ~= oa.oauth_token then
    return mu.returnError(req, "TOKEN mismatched!")
  end
  if not verifier then
    return mu.returnError(req, "VERIFIER is empty!")
  end
  verifier = string.gsub(verifier, "#.*$", "")

  local httpc = http.new()
  local status, err = oa:updateAccessToken(httpc, verifier)
  
  if status then
    req.session.token = oa.oauth_token
    req.session.token_secret = oa.oauth_token_secret
    mycache.setTokenSecret(oa.oauth_token, oa.oauth_token_secret)
    return { redirect_to = req:url_for("index") }
  else
    return mu.returnError(req, "Can't update access token: " .. err)
  end
end

return oauth_confirm