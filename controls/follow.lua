local json = require "cjson"
local basexx = require "basexx"
local tumblr = require "tumblr"
local http = require "resty.http"
local mu = require "myutils"

local function follow(req, isfollow)
  local oa = mu.getOAuth(req)
  local blog = basexx.from_url64(req.params.blog)
  
  local args = { url = blog .. ".tumblr.com" }  
  local res, err;
  if isfollow then
    res, err = tumblr.blogFollow(http.new(), oa, args)
  else
    res, err = tumblr.blogUnfollow(http.new(), oa, args)
  end
  if not res then
    return { json = { status = "FAIL", errmsg = err } }
  end
  
  return { json = { status = "SUCCESS" } }
end

return follow