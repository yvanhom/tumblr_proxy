local mu = require "myutils"
local tumblr = require "tumblr"
local http = require "resty.http"
local json = require "cjson"
local basexx = require "basexx"

local function index(req)
  local oa = mu.getOAuth(req)
  
  if not oa.oauth_token or not oa.oauth_token_secret then
    return { redirect_to = req:url_for("oauth_index") }
  end
  
  local pageid = tonumber(req.params.pid or 0)
  
  local args = { limit = 10 }
  if pageid > 0 then
    args.offset = pageid * 10
  end
  
  local db, err = tumblr.dashboard(http.new(), oa, args)
  if not db then
    return mu.returnError(req, err)
  end
  
  if not db.response or not db.response.posts then
    return nil, "Mismatch format"
  end
  
  local posts, err = tumblr.extractPosts(req, db.response.posts)
  if not posts then
    return mu.returnError(req, err)
  end
  
  req.oa = oa
  req.posts = posts
  req.lastpage = pageid > 0 and req:url_for("index", { pid = pageid - 1 })
  req.nextpage = #posts > 0 and req:url_for("index", { pid = pageid + 1 })

  return { render = "posts" }
end

return index