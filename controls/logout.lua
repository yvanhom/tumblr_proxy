local function logout(req)
  req.session.token = nil
  req.session.token_secret = nil
  
  return { redirect_to = req:url_for("index") }
end

return logout