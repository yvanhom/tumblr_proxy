local mycache = require "mycache"
local myutils = require "myutils"

local function oauth_getcache(req)
  local t, s = mycache.getTokenSecret()
  if not t or not s then
    return myutils.returnError(req, "No cache!")
  end
  
  req.session.token = t
  req.session.token_secret = s
  
  return { redirect_to = req:url_for("index") }
end

return oauth_getcache