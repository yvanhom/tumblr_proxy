local mu = require "myutils"
local http = require "resty.http"
local basexx = require "basexx"

local function proxy(req)
  local url = req.params.url
  url = basexx.from_url64(url)  

  local outpath, err = mu.getFromCache(http.new(), url, "./cache")
  if not outpath then
    print("PROXY ERROR: " .. err)
    return { redirect_to="/static/404.png", status = 404 }
  end
  
  return { redirect_to = "/cache/" .. outpath }
end

return proxy