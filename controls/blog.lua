local basexx = require 'basexx'
local tumblr = require 'tumblr'
local http = require 'resty.http'
local mu = require 'myutils'

local function blog(req)
  local oa = mu.getOAuth(req)
  
  local blogname = req.params.name
  if not blogname or #blogname < 0  then
    return returnError(req, "NO blog name!")
  end
  blogname = basexx.from_url64(blogname)
  
  local pageid = tonumber(req.params.pid or 0)
  local args = { api_key = oa.oauth_consumer_key, limit = 10 }
  if pageid > 0 then
    args.offset = 10 * pageid
  end
  
  local db, err = tumblr.blogPosts(http.new(), oa, blogname, args)
  if not db then
    return mu.returnError(req, err)
  end
  
  if not db.response or not db.response.posts or not db.response.blog then
    return nil, "Mismatch format"
  end
  
  local posts, err = tumblr.extractPosts(req, db.response.posts)
  if not posts then
    return mu.returnError(req, err)
  end
  
  local blog, err = tumblr.extractBlog(req, db.response.blog)
  if not blog then
    return mu.returnError(req, err)
  end
  
  req.blog = blog
  req.posts = posts
  req.oa = oa
  req.lastpage = pageid > 0 and 
    req:url_for("blog", { name = req.params.name, pid = pageid - 1 })
  req.nextpage = #posts > 0 and 
    req:url_for("blog", { name = req.params.name, pid = pageid + 1 })
  
  return { render = "posts" }
end

return blog