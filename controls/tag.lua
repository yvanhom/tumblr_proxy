local basexx = require "basexx"
local tumblr = require "tumblr"
local mu = require "myutils"
local http = require "resty.http"

local function tag(req)
  local oa = mu.getOAuth(req)
  
  if not oa.oauth_token or not oa.oauth_token_secret then
    return { redirect_to = req:url_for("oauth_index") }
  end
  
  local tag = req.params.tag
  if not tag then
    return { redirect_to = req:url_for("index") }
  end
  tag = basexx.from_url64(tag)
  
  local args = { tag = tag }
  local bts = tonumber(req.params.ts or 0)
  if bts > 0 then
    args.before = bts
    args.limit = 10
  end
  
  local db, err = tumblr.tagged(http.new(), oa, args)
  if not db then
    return mu.returnError(req, err)
  end
  
  if not db.response then
    return nil, "Mismatch format"
  end
  
  local posts, mints = tumblr.extractPosts(req, db.response)
  if not posts then
    return mu.returnError(req, mints)
  end
  
  req.posts = posts
  req.oa = oa
  req.searchkey = tag
  if mints then
    req.nextpage = req:url_for("tag", { tag = req.params.tag, ts = mints })
  end
  
  return { render = "posts" }
end

return tag