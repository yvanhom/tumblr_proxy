local basexx = require 'basexx'

local function search(req)
  local key = req.params.key
  if not key then
    return { redirect_to = req:url_for("index") }
  end
  
  key = basexx.to_url64(key)
  return { redirect_to = req:url_for("tag", { tag = key }) }
end

return search