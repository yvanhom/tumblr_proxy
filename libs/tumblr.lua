local json = require 'cjson'
local basexx = require 'basexx'
local util = require("lapis.util")
local _M = {}

local function do_call_oauth(http, oa, method, url, args)
  local last_url, req = oa:buildRequest(method, url, args)
  local res, err = http:request_uri(last_url, req)
  if not res then
    return nil, err
  end
  if tonumber(res.status) ~= 200 then
    print(res.body)
    return nil, "ERROR status: " .. res.status
  end
  
  return json.decode(res.body)
end

local function do_call_key(http, oa, method, url, args)
  args.api_key = oa.oauth_consumer_key
  local query = util.encode_query_string(args)
  local last_url, req
  if method == "GET" then
    last_url = url .. "?" .. query
    req = {
      method = "GET",
      headers = {
        ["content-type"] = "application/x-www-form-urlencoded"
      },
      ssl_verify = false
    }
  else
    last_url = url
    req = {
      method = method,
      headers = {
        ["content-type"] = "application/x-www-form-urlencoded",
        ["content-length"] = #query
      },
      body = query,
      ssl_verify = false
    }
  end
 
  local res, err = http:request_uri(last_url, req)
  if not res then
    return nil, err
  end
  if tonumber(res.status) ~= 200 then
    print(res.body)
    return nil, "ERROR status: " .. res.status
  end
  
  return json.decode(res.body)
end

function _M.dashboard(http, oa, args)
  return do_call_oauth(http, oa, 
    "GET", "https://api.tumblr.com/v2/user/dashboard", 
    args)
end

function _M.tagged(http, oa, args)
  return do_call_oauth(http, oa, 
    "GET", "https://api.tumblr.com/v2/tagged", 
    args)
end

function _M.blogPosts(http, oa, blogname, args)
  local url = "https://api.tumblr.com/v2/blog/" .. blogname .. ".tumblr.com/posts"
  return do_call_oauth(http, oa, 
    "GET", url,
    args)
end

function _M.postLike(http, oa, args)
  return do_call_oauth(http, oa,
    "POST", "https://api.tumblr.com/v2/user/like",
    args)
end

function _M.postUnlike(http, oa, args)
  return do_call_oauth(http, oa,
    "POST", "https://api.tumblr.com/v2/user/unlike",
    args)
end

function _M.blogFollow(http, oa, args)
  return do_call_oauth(http, oa,
    "POST", "https://api.tumblr.com/v2/user/follow",
    args)
end

function _M.blogUnfollow(http, oa, args)
  return do_call_oauth(http, oa,
    "POST", "https://api.tumblr.com/v2/user/unfollow",
    args)
end

local function proxyImage(req, s)
  return string.gsub(s, "<img[^>]+>", function(s)
    local u = string.match(s, "src=[\"\']([^\"\']+)[\"\']")
    if u then
      u = req:url_for("proxy", { url = basexx.to_url64(u) })
      s = string.gsub(s, "src=[\"\'][^\"\']+[\"\']", string.format("src=\"%s\"", u))
    end
    s = string.gsub(s, "<img", "<img class=\"img-responsive img-thumbnail\"")
    return s
  end)
end

function _M.extractPosts(req, srcposts)
  local posts = {}
  local mints
  for _, post in pairs(srcposts) do
    local p = {}
    mints = mints and math.min(mints, post.timestamp) or post.timestamp
    if post.photos then
      for _, photo in ipairs(post.photos) do
        local pht = {}
        if photo.alt_sizes then
          for _, alt in ipairs(photo.alt_sizes) do
            if alt.url and alt.width and alt.width < 600 then
              pht.small = req:url_for("proxy", { url = basexx.to_url64(alt.url) })
              break
            end
          end
        end
        pht.big = photo.original_size and photo.original_size.url
        pht.big = pht.big and req:url_for("proxy", { url = basexx.to_url64(pht.big) })
        pht.small = pht.small or pht.big
        pht.caption = photo.caption and proxyImage(req, photo.caption)
        if pht.big then
          table.insert(p, pht)
        end
      end
    end
    if post.tags and #(post.tags) > 0 then
      p.tags = {}
      for _, tag in ipairs(post.tags) do
        table.insert(p.tags, {
          name = #tag <= 48 and tag or string.sub(tag, 1, 45) .. "...",
          url = req:url_for("tag", { 
            tag = basexx.to_url64(tag)
            })
        })
      end
    end
    if post.title and post.title ~= json.null and #(post.title) > 0 then
      p.title = post.title
    end
    p.body = post.body and proxyImage(req, post.body)
    p.caption = post.caption and proxyImage(req, post.caption)
    if post.type == "answer" then
      p.question = post.question
      p.asking = post.asking_name
      p.answer = proxyImage(req, post.answer)
    end
    p.blog = post.blog_name
    p.followed = post.followed
    p.url = post.post_url
    p.blogurl = req:url_for("blog", { name = basexx.to_url64(p.blog) })
    p.date = string.gsub(post.date, "GMT", "")
    p.likekey = basexx.to_url64(json.encode{ post.id, post.reblog_key })
    p.liked = post.liked
    
    table.insert(posts, p)
  end
  return posts, mints
end

function _M.extractBlog(req, srcblog)
  local b = {}
  b.name = srcblog.name
  b.title = srcblog.title
  b.description = srcblog.description
  b.url = srcblog.url
  b.followed = srcblog.followed
  b.followkey = basexx.to_url64(b.name)
  return b
end

return _M