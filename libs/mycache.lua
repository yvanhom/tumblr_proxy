local mycache = ngx.shared.myshare

local _M = {}

function _M.setTokenSecret(token, secret)
  mycache:set("TOKEN", token)
  mycache:set("SECRET", secret)
end

function _M.getTokenSecret()
  local t = mycache:get("TOKEN")
  local s = mycache:get("SECRET")
  if t and s then
    return t, s
  end
end

return _M