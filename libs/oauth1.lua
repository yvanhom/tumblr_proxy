local encoding = require "lapis.util.encoding"

local OAuth = {}
function OAuth.new(kt, urls, params)
  local t = {
    oauth_consumer_key = kt.key,
    oauth_secret = kt.secret,
    requestTokenURL = urls[1],
    authorizeURL = urls[2],
    accessTokenURL = urls[3],
    callback = params.callback
  }
  setmetatable(t, {
    __index = OAuth
  })
  return t
end

local function generateTimestamp()
  return tostring(math.floor(ngx.now()))
end

local function generateNonce()
  local nonce = tostring(math.random()) .. "random" .. tostring(ngx.now())
  return encoding.encode_base64(encoding.hmac_sha1("aaabbb", nonce))
end

local function oauth_encode(val)
	return val:gsub('[^-._~a-zA-Z0-9]', function(letter)
		return string.format("%%%02x", letter:byte()):upper()
	end)
end

function OAuth:sign(method, url, args, getheader)
  local t = {}
  for k, v in pairs(args) do
    table.insert(t, { 
      key = oauth_encode(k), 
      value = oauth_encode(tostring(v)) 
    })
  end
  table.sort(t, function(a, b)
    if a.key < b.key then
      return true
    elseif a.key > b.key then
      return false
    else
      return a.value < b.value
    end
  end)
  local ts = {}
  for _, kv in ipairs(t) do
    table.insert(ts, kv.key .. '=' .. kv.value)
  end
  local querystr = table.concat(ts, "&")
  local basestr = method .. "&" .. oauth_encode(url) .. "&" .. oauth_encode(querystr)
  local signkey = oauth_encode(self.oauth_secret) .. "&" .. oauth_encode(self.oauth_token_secret or "")
  
  local hmacstr = oauth_encode(encoding.encode_base64(encoding.hmac_sha1(signkey, basestr)))
  
  local headstr
  if getheader then
    local hs = {}
    local qs = {}
    for _, kv in pairs(t) do
      if string.match(kv.key, "^oauth_") then
        table.insert(hs, kv.key .. "=\"" .. kv.value .. "\"")
      else
        table.insert(qs, kv.key .. "=" .. kv.value)
      end
    end
    table.insert(hs, "oauth_signature=\"" ..  hmacstr .. "\"")
    headstr = "OAuth " .. table.concat(hs, ", ")
    querystr = table.concat(qs, "&")
  else
    querystr = querystr .. "&oauth_signature=" .. hmacstr
  end
  
  return hmacstr, querystr, headstr
end

function OAuth:updateRequestToken(http)
  local args = {
    oauth_consumer_key = self.oauth_consumer_key,
    oauth_signature_method = "HMAC-SHA1",
    oauth_timestamp = generateTimestamp(),
    oauth_nonce = generateNonce(),
    oauth_version = "1.0a",
    oauth_callback = self.callback
  }
  
  local _, query = self:sign("POST", self.requestTokenURL, args)
  local body = {}
  local res, err = http:request_uri(self.requestTokenURL, {
    method = "POST",
    headers = {
      ["content-type"] = "application/x-www-form-urlencoded",
      ["content-length"] = #query
    },
    body = query,
    ssl_verify = false
  })
  
  if not res then
    return nil, err
  end
  
  if tonumber(res.status) ~= 200 then
    return nil, "ERROR status " .. tostring(res.status)
  end

  self.oauth_token = string.match(res.body, "oauth_token=([^&]*)&")
  self.oauth_token_secret = string.match(res.body, "oauth_token_secret=([^&]*)&")

  return self
end

function OAuth:getAuthorizeURL()
  if self.oauth_token then
    return self.authorizeURL .. "?oauth_token=" .. self.oauth_token
  end
end

function OAuth:updateAccessToken(http, verifier)
  local args = {
    oauth_consumer_key = self.oauth_consumer_key,
    oauth_token = self.oauth_token,
    oauth_signature_method = "HMAC-SHA1",
    oauth_timestamp = generateTimestamp(),
    oauth_nonce = generateNonce(),
    oauth_version = "1.0a",
    oauth_verifier = verifier
  }
  
  local _, query = self:sign("POST", self.accessTokenURL, args)
  local res, err = http:request_uri(self.accessTokenURL, {
    method = "POST",
    headers = {
      ["content-type"] = "application/x-www-form-urlencoded",
      ["content-length"] = #query
    },
    body = query,
    ssl_verify = false
  })

  if not res then
    return nil, err
  end
  
  if tonumber(res.status) ~= 200 then
    return nil, "ERROR status " .. tostring(res.status)
  end
  
  self.oauth_token = string.match(res.body, "oauth_token=([^&]*)&")
  self.oauth_token_secret = string.match(res.body, "oauth_token_secret=([^&]*)")

  return self
end

function OAuth:buildRequest(method, url, arguments)
  local args = {
    oauth_consumer_key = self.oauth_consumer_key,
    oauth_token = self.oauth_token,
    oauth_signature_method = "HMAC-SHA1",
    oauth_timestamp = generateTimestamp(),
    oauth_nonce = generateNonce(),
    oauth_version = "1.0a"
  }
  if arguments then
    for k, v in pairs(arguments) do
      args[k] = v
    end
  end
  local _, query, hstr = self:sign(method, url, args, true)
  local req
  if method == "GET" then
    if query and #query > 0 then
      url = url .. "?" .. query
    end
    req = {
      method = "GET",
      headers = {
        ["Authorization"] = hstr,
        ["content-type"] = "application/x-www-form-urlencoded"
      },
      ssl_verify = false
    }
  else
    query = query or ""
    req = {
      method = method,
      headers = {
        ["Authorization"] = hstr,
        ["content-type"] = "application/x-www-form-urlencoded",
        ["content-length"] = #query
      },
      body = query,
      ssl_verify = false
    }
  end
  return url, req
end

return OAuth
