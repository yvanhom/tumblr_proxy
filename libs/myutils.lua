local config = require("lapis.config").get()
local oauth = require "oauth1"
local encoding = require "lapis.util.encoding"
local basexx = require "basexx"

local _M = {}
function _M.getOAuth(req)
  local oa = oauth.new(config.oauth, config.oauth_urls, config.oauth_params)

  if req and req.session.token and req.session.token_secret then
    oa.oauth_token = req.session.token
    oa.oauth_token_secret = req.session.token_secret
  end
  
  return oa
end

function _M.returnError(req, msg)
  req.errmsg = msg
  return { render = "error" }
end

function _M.getFromCache(http, url, dir)
  local outpath = basexx.to_url64(encoding.hmac_sha1("aaaBBB", url))
  local prefix = string.match(url, "(%.[^%.]+)$")
  if not prefix or #prefix > 6 then
    prefix = ".data"
  end
  outpath = outpath .. prefix
  local fulloutpath = dir .. "/" .. outpath
  if os.rename(fulloutpath, fulloutpath) then
    print("Hit cache: " .. outpath)
    return outpath
  end    

  print("Request: " .. url)
  local h, host = string.match(url, "^(http.)://([^/]+)")
  local port = (h == "http") and 80 or 443
  local path = string.sub(url, #h + 3 + #host + 1)
  
  http:set_timeout(5000)
  http:connect(host, port)
  
  http:ssl_handshake(true, host, false)
  
  local res, err = http:request({
    path = path,
    ssl_verify = false
  })
  
  if not res then
    return nil, "Can't request: " .. err
  end
  
  local output, err = io.open(fulloutpath, "wb")
  if not output then
    return nil, "Can't open file: " .. err
  end
  
  local reader = res.body_reader
  
  repeat
    local chunk, err = reader(8192)
    if err then
      output:close()
      os.remove(fulloutpath)
      return nil, "Can't read: " .. err
    end
    if chunk then
      output:write(chunk)
    end
  until not chunk
  
  output:close()
  return outpath
end

return _M