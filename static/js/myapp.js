$(function() {
  $('input[id^=like-toggle]').change(function() {
    var checked = this.checked
    var post = $(this).attr("data-toggle-mydata")
    var url = checked ? '/post/like/' : '/post/unlike/'
    $.ajax({
      url: url + post,
      type: 'POST',
      success: function(result) {},
      error: function(result) {}
    })
  })
  $('#follow-toggle').change(function() {
    var checked = this.checked
    var blog = $(this).attr("data-toggle-mydata")
    var url = "/blog/" + blog + (checked ? '/follow' : '/unfollow')
    $.ajax({
      url: url,
      type: 'POST',
      success: function(result) {},
      error: function(result) {}
    })
  })
})